import { StyleSheet } from 'react-native'
import { colors } from '../../theme/colors'

export const style = StyleSheet.create({
    mainView: {
      flex: 1,
      backgroundColor: '#fff',
      width: '100%',
      
    },

    titleView: { 
      backgroundColor: colors.color2,
      width: '100%',
      justifyContent:'flex-end',
      padding: 20,
      flex:1

    },
    titleText: { 
      fontSize: 25, 
      fontWeight:'bold', 
      color: 'white'
    },

    tasksView: {
      width: '100%',
      paddingTop: 20,
      paddingHorizontal: 20,
      flex: 5
    },

    addView: {
      width: '100%',
      paddingHorizontal: 20,
      paddingBottom: 30,
      flex:1
    },
    button: {
      backgroundColor: colors.color3, 
    }
})