import React, { useState } from 'react'
import { View, ScrollView } from 'react-native'
import { Text, Button, Input, CheckBox, Card } from 'react-native-elements'
import { Icon } from 'react-native-elements'
import { style } from './style'
import { colors } from '../../theme/colors'

const Tasks = (props) => {
    const [tasks, setTasks] = useState(props.tasksList || [])
    const [inputText, setInputText] = useState('')

    const addTask = (task) => {
        if (task.length > 0){
            let newTask = { name: task, status: false }
            let tasksUpdated = [...tasks]
            tasksUpdated.push(newTask)
            setTasks(tasksUpdated)
            setInputText('')
        }
    }

    const changeStatusTask = (taskIndex) => {
        let tasksUpdated = [...tasks]
        tasksUpdated[taskIndex].checked = !tasks[taskIndex].checked
        setTasks(tasksUpdated)
    }

    const updateTaskName = (taskIndex, newName) => {
        let tasksUpdated = [...tasks]
        tasksUpdated[taskIndex].name = newName
        setTasks(tasksUpdated)
    }

    const deleteTask = (taskIndex) => {
        let tasksUpdated = [...tasks]
        tasksUpdated = tasksUpdated.filter((value, index) => { return index != taskIndex})
        setTasks(tasksUpdated)
    }

    return (
        <View style={style.mainView}>
            <View style={style.titleView}>
                <Text style={style.titleText}>Liste de tâches de Faly le ouf</Text>
            </View>
            <Card>
                <Text style={{alignSelf: 'flex-end'}}>
                    Tâches restantes : { tasks.filter((value, index) => { return !value.checked }).length }
                </Text>
            </Card>
            <View style={style.tasksView}>
                <ScrollView>
                    { 
                        tasks.map(( task, index ) =>
                            <View key={index} style={{ flexDirection: 'row', justifyContent:'space-between', flexWrap: 'wrap' }}>
                                <Text style={{ fontSize: 15, margin: 1, textDecorationLine: task.checked ? 'line-through': 'none', flex:3 }}> 
                                    { task.name } 
                                </Text>
                                <View style={{ flexDirection: 'row', justifyContent:'flex-end', flex:1 }}>
                                    <CheckBox 
                                        checked={ task.checked } 
                                        onPress={() => { changeStatusTask(index) }}
                                        checkedColor={colors.color2}
                                        containerStyle={{ padding:0, width: 15 }}
                                    />
                                    <Button buttonStyle={{ margin:0, padding:5, backgroundColor:'transparent' }}
                                        icon={
                                            <Icon
                                            name='delete'
                                            type='material'
                                            color={colors.color3}
                                            />
                                        }
                                        onPress={() => deleteTask(index)}
                                    />
                                </View>
                            </View>
                        )
                    }
                </ScrollView>
            </View>
            <View style={style.addView}>
                <Input 
                    placeholder='Nouvelle tâche' 
                    value={inputText} 
                    onSubmitEditing={(event) => {addTask(event.nativeEvent.text)}} 
                    onChangeText={text => setInputText(text)}
                    inputStyle={{ }}
                />
                <Button 
                    title='Ajouter' 
                    onPress={task => addTask(inputText)}
                    buttonStyle={{ backgroundColor: colors.color3 }}
                />
            </View>
        </View>
    )
}

export default Tasks
