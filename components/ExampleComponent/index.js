import React from 'react'
import { Text, View } from 'react-native'
import { styles } from './style'

const ExampleComponent = () => (
    <View style={styles.container}>
      <Text>Open up App.js to start working on your app!</Text>
    </View>
)

export default ExampleComponent
