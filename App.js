import React from 'react'
import { View } from 'react-native'
import ExampleComponent from './components/ExampleComponent'
import Tasks from './components/Tasks'
import * as firebase from 'firebase'

// Initialize Firebase
import { firebaseConfig } from './config'
firebase.initializeApp(firebaseConfig);

const App = () => {
  const tasksList = [
    { name:'Acheter du pain à burger', checked: false },
    { name:'Acheter du fromage pour les burgers', checked: false },
    { name:'Faire les burgers', checked: false },
    { name:'Manger les burgerssssssssssssssssssssssssssssssssssssssssss', checked: false }
  ]

  return (
    <View style={{ flex: 1 }}>
      <Tasks tasksList={tasksList}/>
    </View>
  )
}

export default App
